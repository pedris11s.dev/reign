
## Description

The client app run on `localhost:3001` and server app on `localhost:3000`.
Server endpoints
`GET /feeds`,
`DELETE /feeds/:objectID`

## Start project

```
docker-compose up -d
```