import { Module, HttpModule } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';

import { ScheduleModule } from '@nestjs/schedule';
import { MongooseModule } from '@nestjs/mongoose';
import { FeedsModule } from './feeds/feeds.module';

// blog-mongodb_mongodb_1
const databaseHost = process.env['DATABASE_HOST'] || 'localhost';
const databasePort = process.env['DATABASE_PORT'] || '27017';
const databaseDB = process.env['DATABASE_DB'] || 'nestjs_db';

@Module({
  imports: [
    ScheduleModule.forRoot(),
    MongooseModule.forRoot(`mongodb://${databaseHost}:${databasePort}/${databaseDB}`, { useFindAndModify: false }),
    FeedsModule,
    HttpModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
