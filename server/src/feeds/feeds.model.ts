import * as mongoose from 'mongoose';
import { Long } from 'bson';

export const FeedsSchema = new mongoose.Schema({
  title: { type: String },
  story_title: { type: String },
  url: { type: String },
  story_url: { type: String },
  created_at: { type: String },
  objectID: { type: String },
  author: { type: String },
  deleted: { type: Boolean, default: false },
});

export interface Feed extends mongoose.Document {
  title: string;
  story_title: string;
  url: string;
  story_url: string;
  created_at: string;
  objectID: string;
  author: string;
  deleted: boolean;
}