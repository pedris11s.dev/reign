import { Injectable, NotFoundException, HttpService } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { Feed } from './feeds.model'

import { Cron, Timeout, CronExpression, Interval } from '@nestjs/schedule';

@Injectable()
export class FeedsService {
    constructor(
        private http: HttpService,
        @InjectModel('Feed') private readonly feedModel: Model<Feed>
    ) {}

    @Cron(CronExpression.EVERY_HOUR)
    // @Interval(10000)
    async updateDataFromApi() {
        console.log("updating data from api...");
        const feeds = await this.getFeedsFromApi();
        // console.log(feeds)
        feeds.hits.forEach(element => {
            const query = { objectID: element.objectID };
            const options = { upsert: true, new: true, setDefaultsOnInsert: true };
            this.feedModel.findOneAndUpdate(query, element, options).exec().then(res => {
                // console.log(res);
            });
            // console.log(element.objectID);
        });
        console.log("finish...update data")
    }
    
    @Timeout(1000)
    async loadInitData(){
        const feeds = await this.getFeeds();
        if (feeds.length == 0) {
            console.log('loading data...')
            this.getFeedsFromApi()
                .then(data => {
                    data.hits.forEach(element => {
                        this.insertFeed(element)    
                    });
                })
                .then(res => {
                    console.log('finish...all data its loaded')
                });
        }
    }

    getFeedsFromApi() {
        // const url = 'https://601f9ac6b5a0e9001706a618.mockapi.io/feed';
        const url = 'https://hn.algolia.com/api/v1/search_by_date?query=nodejs';
        return this.http.get(url)
            .toPromise()
            .then(res => res.data)
            .catch(err => {
                console.log(err)
            });
    }

    async insertFeed(feed: Feed) {
        const newFeed = new this.feedModel(feed);
        const result = await newFeed.save();
        return result;
    }

    async getFeeds() {
        let feeds = await this.feedModel.find({deleted: false}).sort({ created_at: "desc" }).exec();
        return feeds;
    }

    async deleteFeed(id: string) {
        const query = { objectID: id };
        const options = { upsert: true, new: true, setDefaultsOnInsert: true };
        const feed = await this.feedModel.findOneAndUpdate(query, { deleted: true }, options).exec();
        if (feed == null)
            throw new NotFoundException("Feed doesn't exist");
            
        return true;
    }
}
