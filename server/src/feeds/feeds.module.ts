import { Module, HttpModule } from '@nestjs/common';

import { MongooseModule } from '@nestjs/mongoose';

import { FeedsController } from './feeds.controller';
import { FeedsService } from './feeds.service';
import { FeedsSchema } from './feeds.model'

@Module({
  controllers: [FeedsController],
  providers: [FeedsService],
  imports: [
    HttpModule,
    MongooseModule.forFeature([{ name: 'Feed', schema: FeedsSchema }])
  ],
})
export class FeedsModule {}
