import { Controller,Get,Param,Delete,HttpStatus } from '@nestjs/common';

import { FeedsService } from './feeds.service'

@Controller('feeds')
export class FeedsController {
    constructor(
        private readonly feedsService: FeedsService
    ) { }

    @Get()
    async getAllFeeds() {
        const feeds = await this.feedsService.getFeeds();
        return feeds;
    }

    @Delete(':id')
    async removeFeed(@Param('id') feedId: string) {
        const isDeleted = await this.feedsService.deleteFeed(feedId);
        if (isDeleted) {
            return {
                statusCode: HttpStatus.OK,
                message: 'Feed deleted successfully',
            };
        }
    }

}
