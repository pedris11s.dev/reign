import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as moment from 'moment';

@Component({
  selector: 'app-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.css']
})

export class FeedComponent implements OnInit {

  feeds: any[]
  globalLoading = true;
  btnClicked = false;

  constructor(
    private http: HttpClient
  ) { }

  ngOnInit(): void {
    this.getFeeds();
  }

  formatDate(dateStr: string) {
    const date = moment(dateStr);
    const diffDays = date.diff(moment.now(), 'days') * -1;
    if(diffDays == 0)
      return date.format('LT');
    else if(diffDays == 1)
      return "Yesterday"
    else
      return date.format('ll').split(",")[0];
  }

  getFeeds() {
    // const url = 'https://hn.algolia.com/api/v1/search_by_date?query=nodejs';
    const url = 'http://localhost:3000/feeds';
    this.http.get<any>(url)
      .subscribe(data => {
        // this.feeds = data.hits;
        this.feeds = data;
        this.feeds.forEach(item => {
          item.created_at = this.formatDate(item.created_at);
        });
        this.globalLoading = false;
      });
  }

  visitUrl(url) {
    if (!this.btnClicked)
      window.open(url, '_blank');

    this.btnClicked = false;
  }

  deleteFeed(id) {
    const url = `http://localhost:3000/feeds/${id}`;
    this.http.delete<any>(url)
      .subscribe(data => {
        this.globalLoading = true;
        this.getFeeds();
      });
    this.btnClicked = true;
  }

}